require "CiderDebugger";
-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------


display.setStatusBar( display.HiddenStatusBar )
system.activate( "multitouch" )

-- require controller module
storyboard = require "storyboard"
canvasController = require "canvasController"
navController = require "navController"

--define some globals
screen = 
{
    left = display.screenOriginX,
    top = display.screenOriginY,
    right = display.contentWidth - display.screenOriginX,
    bottom = display.contentHeight - display.screenOriginY
};

_G.Dispatcher = display.newGroup()
    

-- load first screen
--storyboard.gotoScene( "loading" ) 
navController.loadSideNavUI()
