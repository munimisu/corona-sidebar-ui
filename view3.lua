module(..., package.seeall)
----------------------------------------------------------------------------------
--
-- view1.lua
--
----------------------------------------------------------------------------------


--VARS
local modGroup

function loadView(group)
    modGroup = display.newGroup()
    --ADD UI
    local view3Txt = display.newText(modGroup, "View 3", 0,0, 0, 0, native.systemFontBold, 20)
    view3Txt:setReferencePoint(display.CenterReferencePoint)
    view3Txt.x=(screen.right-screen.left)/2
    view3Txt.y=70
    view3Txt:setTextColor(118,118,118)
    
    group:insert(modGroup)
end

function destroyView()
    print('Destroying view...')
    if(modGroup~=nil)then
        modGroup:removeSelf()
        modGroup=nil 
    end
end


