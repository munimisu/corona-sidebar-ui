module (..., package.seeall)
----------------------------------------------------------------------------------
--
-- navController.lua
--
----------------------------------------------------------------------------------

--VARS
local mainGroup = display.newGroup()
local view1 = require('view1')
local view2 = require('view2')
local view3 = require('view3')
local curView
local button1
local button2
local button3


local function returnButtonsStart()
    transition.to(button1,{time=200,x=screen.left})
    transition.to(button2,{time=200,x=screen.left})
    transition.to(button3,{time=200,x=screen.left})
end

local function loadView1(event)
    if(event.phase=='ended')then
        print('Loading View1...') 
        returnButtonsStart()
        transition.to(button1,{time=200,x=screen.left+25})
        canvasController.loadView(view1)
    end
end

local function loadView2(event)
    if(event.phase=='ended')then
        print('Loading View2...') 
        returnButtonsStart()
        transition.to(button2,{time=200,x=screen.left+25})
        canvasController.loadView(view2)
    end
end

local function loadView3(event)
    if(event.phase=='ended')then
        print('Loading View3...') 
        returnButtonsStart()
        transition.to(button3,{time=200,x=screen.left+25})
        canvasController.loadView(view3)
    end
end

function loadSideNavUI()
    local sidenavGroup = display.newGroup()
    
    --ADD A BKG
    local navBkg = display.newRect(sidenavGroup, screen.left,screen.top,screen.right-screen.left,screen.bottom-screen.top)
    navBkg:setFillColor(18,112,169)
    
    --ADD SOME NAV BUTTONS
    button1 = display.newGroup()
    
    local button1Bkg = display.newRect(button1, screen.left, screen.top+200, screen.right-screen.left, 45)
    button1Bkg:setReferencePoint(display.TopLeftReferencePoint)
    button1Bkg:setFillColor(237,237,237)
    button1Bkg:setStrokeColor(77,77,77)
    button1Bkg.strokeWidth=2
    button1Bkg:addEventListener( "touch", loadView1 )
    
    local view1Txt = display.newText(button1, "View 1", screen.left+10, button1Bkg.y+12, 0, 0, native.systemFontBold, 20)
    view1Txt:setTextColor(118,118,118)
    
    sidenavGroup:insert(button1)
    
    button2 = display.newGroup()
    
    local button2Bkg = display.newRect(button2, screen.left, button1Bkg.y+45, screen.right-screen.left, 45)
    button2Bkg:setReferencePoint(display.TopLeftReferencePoint)
    button2Bkg:setFillColor(237,237,237)
    button2Bkg:setStrokeColor(77,77,77)
    button2Bkg.strokeWidth=2
    button2Bkg:addEventListener( "touch", loadView2 )
    
    local view2Txt = display.newText(button2, "View 2", screen.left+10, button2Bkg.y+12, 0, 0, native.systemFontBold, 20)
    view2Txt:setTextColor(118,118,118)
    
    sidenavGroup:insert(button2)
    
    button3 = display.newGroup()
    
    local button3Bkg = display.newRect(button3, screen.left, button2Bkg.y+45, screen.right-screen.left, 45)
    button3Bkg:setReferencePoint(display.TopLeftReferencePoint)
    button3Bkg:setFillColor(237,237,237)
    button3Bkg:setStrokeColor(77,77,77)
    button3Bkg.strokeWidth=2
    button3Bkg:addEventListener( "touch", loadView3 )
    
    local view3Txt = display.newText(button3, "View 3", screen.left+10, button3Bkg.y+12, 0, 0, native.systemFontBold, 20)
    view3Txt:setTextColor(118,118,118)
    
    sidenavGroup:insert(button3)
    
    mainGroup:insert(sidenavGroup)
    
    canvasController.init(mainGroup)
    
    --LOAD THE FIRST VIEW BY DEFAULT
    local dummyEvent={phase="ended"}
    loadView1(dummyEvent)
end
